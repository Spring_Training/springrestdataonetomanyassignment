package RestData.Repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import RestData.Models.Manager;
@RepositoryRestResource(collectionResourceRel = "manager", path = "manager")
public interface ManagerRepository extends PagingAndSortingRepository<Manager, Integer> {
}