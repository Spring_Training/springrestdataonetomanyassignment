
package RestData.Controllers;

import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestBody;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;  
import org.springframework.web.bind.annotation.RestController;

import RestData.Models.Manager;
import RestData.Services.ManagerService;

import java.util.List;  
import java.util.Optional;  

@RestController 
public class ManagerController {
 
	    @Autowired  
	    private ManagerService managerService;   
	    @RequestMapping("/")  
	    public List<Manager> getAllManager(){  
	        return managerService.getAllManager();  
	    }     
	    @RequestMapping(value="/add-manager", method=RequestMethod.POST)  
	    public void addManager(@RequestBody Manager manager){  
	        managerService.addManager(manager);  
	    }  
	    @RequestMapping(value="/manager/{id}", method=RequestMethod.GET)  
	    public Optional<Manager> getManager(@PathVariable Integer id){  
	        return managerService.getManager(id);  
	    }  
	}  

