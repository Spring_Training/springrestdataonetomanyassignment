package RestData.Controllers;

import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestBody;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;  
import org.springframework.web.bind.annotation.RestController;

import RestData.Models.Employee;
import RestData.Services.EmployeeService;

import java.util.List;  
import java.util.Optional;  

@RestController  
public class EmployeeController {

	    @Autowired  
	    private EmployeeService employeeService;   
	    @RequestMapping("/")  
	    public List<Employee> getAllEmployee(){  
	        return employeeService.getAllEmployee();  
	    }     
	    @RequestMapping(value="/add-employee", method=RequestMethod.POST)  
	    public void addEmployee(@RequestBody Employee employee){  
	        employeeService.addEmployee(employee);  
	    }  
	    @RequestMapping(value="/employee/{id}", method=RequestMethod.GET)  
	    public Optional<Employee> getEmployee(@PathVariable Integer id){  
	        return employeeService.getEmployee(id);  
	    }  
	}  

