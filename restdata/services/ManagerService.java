package RestData.Services;

import java.util.List;  
import java.util.Optional;  
import java.util.ArrayList;  
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Service;

import RestData.Models.Manager;
import RestData.Repositories.ManagerRepository;

@Service
public class ManagerService {

	@Autowired  
    private ManagerRepository managerRepository;  
    public List<Manager> getAllManager(){  
        List<Manager>manager = new ArrayList<>();  
        managerRepository.findAll().forEach(manager::add);  
        return manager;  
    }  
    public Optional<Manager> getManager(Integer id){  
        return managerRepository.findById(id);  
    }  
    public void addManager(Manager manager){  
        managerRepository.save(manager);  
    }  
    public void delete(Integer id){  
        managerRepository.deleteById(id);  
    }  
}  