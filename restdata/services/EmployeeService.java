package RestData.Services;

import java.util.List;  
import java.util.Optional;  
import java.util.ArrayList;  
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Service;

import RestData.Models.Employee;
import RestData.Repositories.EmployeeRepository;

@Service
public class EmployeeService {

    @Autowired  
    private EmployeeRepository employeeRepository;  
    public List<Employee> getAllEmployee(){  
        List<Employee>employee = new ArrayList<>();  
        employeeRepository.findAll().forEach(employee::add);  
        return employee;  
    }  
    public Optional<Employee> getEmployee(Integer id){  
        return employeeRepository.findById(id);  
    }  
    public void addEmployee(Employee employee){  
        employeeRepository.save(employee);  
    }  
    public void delete(Integer id){  
        employeeRepository.deleteById(id);  
    }  
}  