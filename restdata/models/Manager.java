package RestData.Models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Column;
 
@Entity
public class Manager {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Column(nullable = false)
    private String name;
    
    @OneToMany(mappedBy = "manager", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Employee> employee;
    
    public Manager(){
    }
    
    public Manager(String name){
    	this.name = name;
    }
    
    public Manager(String name, Set<Employee> employee){
    	this.name = name;
    	this.employee = employee;
    }
    
    // name
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    // employees
    public void setEmployee(Set<Employee> employee){
    	this.employee = employee;
    }
    
    public Set<Employee> getEmployee(){
    	return this.employee;
    }
}