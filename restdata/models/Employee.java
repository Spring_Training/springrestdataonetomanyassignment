package RestData.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Employee {

	@Id
   @GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Column
   private String name;
   
   @ManyToOne(fetch = FetchType.EAGER)
   @JoinColumn(name="manager_id")
   private Manager manager;
   
   public Employee(){
   }
   
   public Employee(String name){
   	this.name = name;
   }
   
   public Employee(String name, Manager manager){
   	this.name = name;
   	this.manager = manager;
   }
   
   // name
   public String getName() {
       return name;
   }
   
   public void setName(String name) {
       this.name = name;
   }
   
   // Manager
   public void setManager(Manager manager){
   	this.manager = manager;
   }
   
   public Manager getManager(){
   	return this.manager;
   }
}
