package RestData;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication; 

@SpringBootApplication
public class SpringRestDataOneToMany_AssignmentApplication {
 
	public static void main(String[] args) {
		SpringApplication.run(SpringRestDataOneToMany_AssignmentApplication.class, args);
	}
}
	